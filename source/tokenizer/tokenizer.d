module tokenizer.tokenizer;

import tokenizer.token;
import std.regex;
import std.format;
import std.typecons;
import std.algorithm.searching;
import std.string;
import std.conv : to;
import error.errorHandler;

/**
 * A lazy tokenizer
 */
class Tokenizer {

	private string source;
	private int cursor = 0;
	private int line = 1;
	private auto rules = [
		tuple(ctRegex!(`^\d+(\.\d+)?`), TokenType.NUMBER),
		tuple(ctRegex!(`^"[^"]*"`), TokenType.STRING),
		tuple(ctRegex!(`^;`), TokenType.SEPARATOR),
		tuple(ctRegex!(`^\.`), TokenType.DOT),
		tuple(ctRegex!(`^,`), TokenType.COMMA),
		tuple(ctRegex!(`^:`), TokenType.COLON),
		tuple(ctRegex!(`^let\b`), TokenType.VARIABLE_DECL),
		tuple(ctRegex!(`^mut\b`), TokenType.MUT),
		tuple(ctRegex!(`^nil\b`), TokenType.NIL),
		tuple(ctRegex!(`^fun\b`), TokenType.FUNCTION_DECL),
		tuple(ctRegex!(`^if\b`), TokenType.IF),
		tuple(ctRegex!(`^while\b`), TokenType.WHILE),
		tuple(ctRegex!(`^foreach\b`), TokenType.FOREACH),
		tuple(ctRegex!(`^of\b`), TokenType.OF),
		tuple(ctRegex!(`^else\b`), TokenType.ELSE),
		tuple(ctRegex!(`^return\b`), TokenType.RETURN),
		tuple(ctRegex!(`^break\b`), TokenType.BREAK),
		tuple(ctRegex!(`^(true|false)\b`), TokenType.BOOLEAN),
		tuple(ctRegex!(`^\(`), TokenType.OPEN_PAR),
		tuple(ctRegex!(`^\)`), TokenType.CLOSED_PAR),
		tuple(ctRegex!(`^\{`), TokenType.OPEN_BRACE),
		tuple(ctRegex!(`^\}`), TokenType.CLOSED_BRACE),
		tuple(ctRegex!(`^\[`), TokenType.OPEN_BRACKET),
		tuple(ctRegex!(`^\]`), TokenType.CLOSED_BRACKET),
		tuple(ctRegex!(`^[a-zA-Z][a-zA-Z_0-9]*`), TokenType.IDENTIFIER),
		tuple(ctRegex!(`^\/\/.*`), TokenType.BLANK),
		tuple(ctRegex!(`^\/\*[\s\S]*?\*\/.*`), TokenType.BLANK),
		tuple(ctRegex!(`^\+\+`), TokenType.INC),
		tuple(ctRegex!(`^\+=`), TokenType.ASSIGN_OP),
		tuple(ctRegex!(`^\*=`), TokenType.ASSIGN_OP),
		tuple(ctRegex!(`^\/=`), TokenType.ASSIGN_OP),
		tuple(ctRegex!(`^%=`), TokenType.ASSIGN_OP),
		tuple(ctRegex!(`^\-=`), TokenType.ASSIGN_OP),
		tuple(ctRegex!(`^\+`), TokenType.PLUS),
		tuple(ctRegex!(`^\-\-`), TokenType.DEC),
		tuple(ctRegex!(`^\-`), TokenType.MINUS),
		tuple(ctRegex!(`^\*`), TokenType.MULT),
		tuple(ctRegex!(`^\/`), TokenType.DIV),
		tuple(ctRegex!(`^==`), TokenType.EQUAL),
		tuple(ctRegex!(`^!=`), TokenType.NOT_EQUAL),
		tuple(ctRegex!(`^%`), TokenType.MOD),
		tuple(ctRegex!(`^<=`), TokenType.LESS_EQ),
		tuple(ctRegex!(`^>=`), TokenType.GREATER_EQ),
		tuple(ctRegex!(`^<`), TokenType.LESS),
		tuple(ctRegex!(`^>`), TokenType.GREATER),
		tuple(ctRegex!(`^&&`), TokenType.AND),
		tuple(ctRegex!(`^=`), TokenType.ASSIGN_OP),
		tuple(ctRegex!(`^\!`), TokenType.NOT),
		tuple(ctRegex!(`^\|\|`), TokenType.OR),
		tuple(ctRegex!(`^\s+`), TokenType.BLANK)
	];

	this(string source) {
		this.source = source;
	}

	private bool hasMoreTokens() {
		return cursor < source.length;
	}

	/**
	 * Returns the next token (or null if none) of the stream
	 *
	 * Throws: Exception if in encounters an unknown token
	 * Returns: The next token of the stream or null if there is none
	 */
	public Token getNextToken() {
		if (!hasMoreTokens()) {
			return Token(TokenType.EOF, "eof", line, cursor);
		}
		auto string = source[cursor .. $];

		foreach (rule; rules) {
			auto m = string.matchFirst(rule[0]);

			// does not match this rule, skip to the next
			if (m.empty) {
				continue;
			}

			auto value = m.front;
			cursor += value.length;
			auto newLineIdx = source[0 .. cursor].lastIndexOf("\n");
			newLineIdx = newLineIdx == -1 ? 0 : newLineIdx;
			auto column = to!int(source[newLineIdx .. cursor].indexOf(value));
			line += value.count("\n");

			// skipping blank characters or comments
			if (rule[1] == TokenType.BLANK) {
				return getNextToken();
			}

			return Token(rule[1], value.strip, line, column);
		}
		auto newLineIdx = source[0 .. cursor].lastIndexOf("\n");
		newLineIdx = newLineIdx == -1 ? 0 : newLineIdx;
		auto column = to!int(cursor++ - newLineIdx);
		return Token(TokenType.UNKNOWN, "" ~ source[cursor - 1], line, column);
	}

	/// It correctly tokenizes a number
	unittest {
		auto tokenizer = new Tokenizer("123");
		auto token = tokenizer.getNextToken();
		assert(token.value == "123" && token.type == TokenType.NUMBER);
	}

	/// It correctly tokenizes a number
	unittest {
		auto tokenizer = new Tokenizer("nil");
		auto token = tokenizer.getNextToken();
		assert(token.value == "nil" && token.type == TokenType.NIL);
	}

	/// It correctly tokenizes if and else
	unittest {
		auto tokenizer = new Tokenizer("if else");
		auto token = tokenizer.getNextToken();
		assert(token.value == "if" && token.type == TokenType.IF);
		token = tokenizer.getNextToken();
		assert(token.value == "else" && token.type == TokenType.ELSE);
	}

	/// It correctly tokenizes loops
	unittest {
		auto tokenizer = new Tokenizer("while foreach of");
		auto token = tokenizer.getNextToken();
		assert(token.value == "while" && token.type == TokenType.WHILE);
		token = tokenizer.getNextToken();
		assert(token.value == "foreach" && token.type == TokenType.FOREACH);
		token = tokenizer.getNextToken();
		assert(token.value == "of" && token.type == TokenType.OF);
	}

	/// It correctly tokenizes return and break
	unittest {
		auto tokenizer = new Tokenizer("return break");
		auto token = tokenizer.getNextToken();
		assert(token.value == "return" && token.type == TokenType.RETURN);
		token = tokenizer.getNextToken();
		assert(token.value == "break" && token.type == TokenType.BREAK);
	}

	/// It correctly tokenises a string
	unittest {
		auto tokenizer = new Tokenizer("\"hello\"");
		auto token = tokenizer.getNextToken();
		assert(token.value == "\"hello\"" && token.type == TokenType.STRING);
	}

	/// It correctly ignores blank characters
	unittest {
		auto tokenizer = new Tokenizer("
			13
  			 ");
		auto token = tokenizer.getNextToken();
		assert(token.column == 4);
		assert(token.line == 2);
		assert(token.value == "13" && token.type == TokenType.NUMBER);
	}

	/// It correctly ignores single line comments and blank characters
	unittest {
		auto tokenizer = new Tokenizer("
			// this is a comment
			12
			");
		auto token = tokenizer.getNextToken();
		assert(token.value == "12" && token.type == TokenType.NUMBER);
		assert(token.line == 3);
	}

	/// It correctly ignores multi lines comments and blank characters
	unittest {
		auto tokenizer = new Tokenizer("   /*
    * this is a comment
    */
    12
");
		auto token = tokenizer.getNextToken();
		assert(token.value == "12");
		assert(token.type == TokenType.NUMBER);
		assert(token.line == 4);
		assert(token.column == 5);
	}

	/// It correctly tokenises maths operators
	unittest {
		auto tokenizer = new Tokenizer("+-%/*");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.PLUS && token.value == "+");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.MINUS && token.value == "-");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.MOD && token.value == "%");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.DIV && token.value == "/");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.MULT && token.value == "*");
	}

	/// It correctly tokenises semicolon and comma
	unittest {
		auto tokenizer = new Tokenizer(",;.:");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.COMMA);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.SEPARATOR);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.DOT);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.COLON);
	}

	/// It correctly tokenises increment and decrement
	unittest {
		auto tokenizer = new Tokenizer("++--");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.INC && token.value == "++");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.DEC && token.value == "--");
	}

	/// It correctly tokenises boolean operators
	unittest {
		auto tokenizer = new Tokenizer("<= < > >= && || == !=");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.LESS_EQ && token.value == "<=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.LESS && token.value == "<");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.GREATER && token.value == ">");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.GREATER_EQ && token.value == ">=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.AND && token.value == "&&");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.OR && token.value == "||");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.EQUAL && token.value == "==");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.NOT_EQUAL && token.value == "!=");
	}

	/// It correctly tokenises parenthesis and brackets
	unittest {
		auto tokenizer = new Tokenizer("(){}[]");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.OPEN_PAR);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.CLOSED_PAR);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.OPEN_BRACE);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.CLOSED_BRACE);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.OPEN_BRACKET);
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.CLOSED_BRACKET);
	}

	/// It correctly tokenizes an identifier
	unittest {
		auto tokenizer = new Tokenizer("myVar_9");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.IDENTIFIER);
	}

	/// It correctly tokenizes the assignment operator
	unittest {
		auto tokenizer = new Tokenizer("= += -= *= /= %=");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.ASSIGN_OP);
		assert(token.value == "=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.ASSIGN_OP);
		assert(token.value == "+=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.ASSIGN_OP);
		assert(token.value == "-=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.ASSIGN_OP);
		assert(token.value == "*=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.ASSIGN_OP);
		assert(token.value == "/=");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.ASSIGN_OP);
		assert(token.value == "%=");
	}

	/// It correctly tokenizes the assignment declarator
	unittest {
		auto tokenizer = new Tokenizer("let mut");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.VARIABLE_DECL);
		assert(token.value == "let");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.MUT);
		assert(token.value == "mut");
	}

	/// It correctly tokenizes the function declarator
	unittest {
		auto tokenizer = new Tokenizer("fun");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.FUNCTION_DECL);
		assert(token.value == "fun");
	}

	/// It correctly tokenizes boolean litterals
	unittest {
		auto tokenizer = new Tokenizer("true false");
		auto token = tokenizer.getNextToken();
		assert(token.type == TokenType.BOOLEAN);
		assert(token.value == "true");
		token = tokenizer.getNextToken();
		assert(token.type == TokenType.BOOLEAN);
		assert(token.value == "false");
	}

}

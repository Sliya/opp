module tokenizer.token;

import std.conv;
import vibe.data.json;

enum TokenType {
	STRING,
	NUMBER,
	BOOLEAN,
	SEPARATOR,
	EQUAL,
	NOT_EQUAL,
	GREATER,
	GREATER_EQ,
	LESS,
	LESS_EQ,
	AND,
	OR,
	MOD,
	NOT,
	DIV,
	INC,
	DEC,
	MULT,
	PLUS,
	MINUS,
	COMMA,
	COLON,
	ASSIGN_OP,
	VARIABLE_DECL,
	MUT,
	FUNCTION_DECL,
	IDENTIFIER,
	OPEN_PAR,
	CLOSED_PAR,
	OPEN_BRACE,
	CLOSED_BRACE,
	OPEN_BRACKET,
	CLOSED_BRACKET,
	IF,
	ELSE,
	WHILE,
	FOREACH,
	OF,
	RETURN,
	BREAK,
	DOT,
	EOF,
	UNKNOWN,
	NIL,
	BLANK
}

struct Token {
	private TokenType m_type;
	private string m_value;
	private int m_line;
	private int m_column;

	this(TokenType type, string value, int line, int column) {
		this.m_type = type;
		this.m_value = value;
		this.m_line = line;
		this.m_column = column;
	}

	@property TokenType type() {
		return m_type;
	}

	@property string value() {
		return m_value;
	}

	@property int line() {
		return m_line;
	}

	@property int column() {
		return m_column;
	}

	string toString() {
		return this.serializeToJson().toPrettyString();
	}
}

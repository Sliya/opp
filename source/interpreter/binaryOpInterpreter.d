module interpreter.binaryOpInterpreter;

import std.conv : to;
import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.value;
import interpreter;
import interpreter.environment;
import std.format;

class BinaryOpInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type = NodeType.BINARY_OP);
	}
	do {
		auto left = interpreter.interpret(ast.left, env);
		auto right = interpreter.interpret(ast.right, env);
		if (left.type != right.type) {
			throw new Exception(
					format("Runtime exception: Binary operations' operands must be of the same type. Got %s and %s", left
					.type, right.type));
		}
		switch (ast.value) {
		case "+":
			checkType(left, [ValueType.NUMBER, ValueType.STRING, ValueType.ARRAY]);
			if (left.type == ValueType.NUMBER) {
				return Value(ValueType.NUMBER, to!string(left.numberValue() + right.numberValue()));
			} else if (left.type == ValueType.ARRAY) {
				return Value.array(left.values ~ right.values);
			} else {
				return Value(ValueType.STRING, left.stringValue() ~ right.stringValue());
			}
		case "-":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.NUMBER, to!string(left.numberValue() - right.numberValue()));
		case "*":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.NUMBER, to!string(left.numberValue() * right.numberValue()));
		case "%":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.NUMBER, to!string(left.numberValue() % right.numberValue()));
		case "/":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.NUMBER, to!string(left.numberValue() / right.numberValue()));
		case "&&":
			checkType(left, ValueType.BOOLEAN);
			return Value(ValueType.BOOLEAN, to!string(left.boolValue && right.boolValue));
		case "||":
			checkType(left, ValueType.BOOLEAN);
			return Value(ValueType.BOOLEAN, to!string(left.boolValue() || right.boolValue()));
		case "<":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.BOOLEAN, to!string(left.numberValue() < right.numberValue()));
		case "<=":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.BOOLEAN, to!string(left.numberValue() <= right.numberValue()));
		case ">":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.BOOLEAN, to!string(left.numberValue() > right.numberValue()));
		case ">=":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.BOOLEAN, to!string(left.numberValue() >= right.numberValue()));
		case "==":
			return Value(ValueType.BOOLEAN, to!string(left.value == right.value));
		case "!=":
			return Value(ValueType.BOOLEAN, to!string(left.value != right.value));
		default:
			throw new Exception("Unknown binary operator " ~ ast.value);
		}
	}

	// Correctly interprets an addition
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "2");
		auto binOp = new AST(NodeType.BINARY_OP, "+", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 10);
	}

	// Correctly interprets string concatenation
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.STRING, "\"a\"");
		auto right = new AST(NodeType.STRING, "\"b\"");
		auto binOp = new AST(NodeType.BINARY_OP, "+", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.STRING);
		assert(value.stringValue == "ab");
	}

	// Correctly interprets array concatenation
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.ARRAY, [new AST(NodeType.NUMBER, "1")]);
		auto right = new AST(NodeType.ARRAY, [new AST(NodeType.NUMBER, "2")]);
		auto binOp = new AST(NodeType.BINARY_OP, "+", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.ARRAY);
		assert(value.toString == "[1, 2]");
	}

	// Correctly interprets a substraction
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "2");
		auto binOp = new AST(NodeType.BINARY_OP, "-", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 6);
	}

	// Correctly interprets a multiplication
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "2");
		auto binOp = new AST(NodeType.BINARY_OP, "*", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 16);
	}

	// Correctly interprets a division
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "2");
		auto binOp = new AST(NodeType.BINARY_OP, "/", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 4);
	}

	// Correctly interprets a modulo
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "2");
		auto binOp = new AST(NodeType.BINARY_OP, "%", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 0);
	}

	// Correctly interprets a AND
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.BOOLEAN, "true");
		auto right = new AST(NodeType.BOOLEAN, "false");
		auto binOp = new AST(NodeType.BINARY_OP, "&&", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "&&", left, left);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
	}

	// Correctly interprets a OR
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.BOOLEAN, "true");
		auto right = new AST(NodeType.BOOLEAN, "false");
		auto binOp = new AST(NodeType.BINARY_OP, "||", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "||", right, right);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
	}

	// Correctly interprets a smaller than comparison
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "20");
		auto binOp = new AST(NodeType.BINARY_OP, "<", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "<", right, left);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
	}

	// Correctly interprets a smaller than or equal to comparison
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "10");
		auto right = new AST(NodeType.NUMBER, "20");
		auto binOp = new AST(NodeType.BINARY_OP, "<=", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "<=", right, left);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "<=", right, right);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
	}

	// Correctly interprets a greater than comparison
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "2");
		auto binOp = new AST(NodeType.BINARY_OP, ">", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, ">", right, left);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
	}

	// Correctly interprets a greater than or equal to comparison
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto right = new AST(NodeType.NUMBER, "20");
		auto binOp = new AST(NodeType.BINARY_OP, ">=", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, ">=", right, left);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, ">=", right, right);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
	}

	// Correctly interprets a equal comparison
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "2");
		auto right = new AST(NodeType.NUMBER, "20");
		auto binOp = new AST(NodeType.BINARY_OP, "==", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "==", right, right);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
	}

	// Correctly interprets a not equal comparison
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "2");
		auto right = new AST(NodeType.NUMBER, "20");
		auto binOp = new AST(NodeType.BINARY_OP, "!=", left, right);
		auto inter = new BinaryOpInterpreter();
		auto value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(value.boolValue);
		binOp = new AST(NodeType.BINARY_OP, "!=", right, right);
		value = inter.interpret(binOp, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
	}
}

module interpreter.booleanInterpreter;

import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.value;
import std.conv : to;
import interpreter.environment;
import interpreter.interpreter;

class BooleanInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.BOOLEAN);
	}
	do {
		return Value(ValueType.BOOLEAN, to!string(ast.boolValue));
	}

	/// Correctly interprets a boolean litteral
	unittest {
		auto env = new Environment();
		auto ast = new AST(NodeType.BOOLEAN, "true");
		auto inter = new BooleanInterpreter();
		assert(inter.interpret(ast, env).boolValue);
		ast = new AST(NodeType.BOOLEAN, "false");
		assert(!inter.interpret(ast, env).boolValue);
	}
}

module interpreter.interpreter;

import std.stdio : File;
import std.file;
import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.value;
import interpreter.blockInterpreter;
import interpreter.binaryOpInterpreter;
import interpreter.numberInterpreter;
import interpreter.stringInterpreter;
import interpreter.identifierInterpreter;
import interpreter.declarationInterpreter;
import interpreter.assignmentInterpreter;
import interpreter.booleanInterpreter;
import interpreter.nilInterpreter;
import interpreter.ifElseInterpreter;
import interpreter.whileInterpreter;
import interpreter.unaryOpInterpreter;
import interpreter.functionDeclarationInterpreter;
import interpreter.functionCallInterpreter;
import interpreter.returnInterpreter;
import interpreter.breakInterpreter;
import interpreter.arrayInterpreter;
import interpreter.arrayAccessInterpreter;
import interpreter.objectInterpreter;
import interpreter.environment;
import parser;

class Interpreter {

	File output;
	private Environment env;
	private bool stdlibLoaded = false;

	// A map of NodeTypes and the associated interpreter
	private NodeInterpreter[NodeType] interpreters;

	this(File output) {
		env = new Environment();
		this.output = output;
		interpreters = [
			NodeType.BLOCK: new BlockInterpreter(this),
			NodeType.BINARY_OP: new BinaryOpInterpreter(this),
			NodeType.RETURN: new ReturnInterpreter(this),
			NodeType.BREAK: new BreakInterpreter(this),
			NodeType.NIL: new NilInterpreter(this),
			NodeType.IDENTIFIER: new IdentifierInterpreter(this),
			NodeType.DECLARATION: new DeclarationInterpreter(this),
			NodeType.ARRAY: new ArrayInterpreter(this),
			NodeType.OBJECT: new ObjectInterpreter(this),
			NodeType.ARRAY_ACCESS: new ArrayAccessInterpreter(this),
			NodeType.FUNCTION_DECLARATION: new FunctionDeclarationInterpreter(this),
			NodeType.FUNCTION_CALL: new FunctionCallInterpreter(this),
			NodeType.ASSIGNMENT: new AssignmentInterpreter(this),
			NodeType.IF: new IfElseInterpreter(this),
			NodeType.WHILE: new WhileInterpreter(this),
			NodeType.NUMBER: new NumberInterpreter(this),
			NodeType.BOOLEAN: new BooleanInterpreter(this),
			NodeType.STRING: new StringInterpreter(this),
			NodeType.UNARY_OP: new UnaryOpInterpreter(this)
		];
	}

	/**
	  * Interprets the given AST in the given environment and returns the computed value
	  *
	  * Returns: the computed value of the AST
	  */
	public Value interpret(AST ast, ref Environment env) {
		if (!stdlibLoaded) {
			stdlibLoaded = true;
			interpret(new Parser().parse(readText("stdlib/arrays.opp")), env);
			interpret(new Parser().parse(readText("stdlib/strings.opp")), env);
		}
		return interpreters[ast.type].interpret(ast, env);
	}
}

module interpreter.breakException;

import interpreter.value;

class BreakException : Exception {
	Value _value;
	this(Value v, string file = __FILE__, size_t line = __LINE__) {
		super(v.value, file, line);
		this._value = v;
	}

	@property Value value() {
		return _value;
	}
}

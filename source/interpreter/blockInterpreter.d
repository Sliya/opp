module interpreter.blockInterpreter;

import interpreter.nodeInterpreter;
import interpreter.value;
import parser.ast;
import interpreter;
import interpreter.environment;

class BlockInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	/**
	  * Interprets the given AST and returns the computed value of the last expression of the block
	  *
	  * Returns: the computed value of the last expression of the block
	  */
	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.BLOCK);
	}
	do {
		Environment oldEnv = env;
		if (ast.value == "block") {
			env = new Environment(env);
		}

		Value v;
		foreach (expression; ast.collection) {
			v = interpreter.interpret(expression, env);
		}
		env = oldEnv;
		return v;
	}

	/// Correctly interprets a block and returns the value of the last expression
	unittest {
		auto env = new Environment();
		auto collection = [new AST(NodeType.NUMBER, "3"), new AST(NodeType.NUMBER, "4")];
		auto block = new AST(NodeType.BLOCK, "block", collection);
		auto inter = new BlockInterpreter();
		assert(inter.interpret(block, env).numberValue == 4);
	}

	/// Correctly interprets a program block and does not create a scoped environment
	unittest {
		auto env = new Environment();
		auto collection = [
			new AST(NodeType.ASSIGNMENT, "=", new AST(NodeType.IDENTIFIER, "foo"), new AST(NodeType.NUMBER, "4"))
		];
		auto block = new AST(NodeType.BLOCK, "program", collection);
		auto inter = new BlockInterpreter();
		inter.interpret(block, env);
		try {
			env.get("foo");
		} catch (Exception e) {
			assert(false);
		}
	}

	/// Correctly interprets a block and creates a scoped environment
	unittest {
		auto env = new Environment();
		auto collection = [
			new AST(NodeType.ASSIGNMENT, "=", new AST(NodeType.IDENTIFIER, "foo"), new AST(NodeType.NUMBER, "4"))
		];
		auto block = new AST(NodeType.BLOCK, "block", collection);
		auto inter = new BlockInterpreter();
		try {
			env.get("foo");
			assert(false);
		} catch (Exception e) {
		}
	}
}

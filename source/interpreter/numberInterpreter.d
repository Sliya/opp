module interpreter.numberInterpreter;

import interpreter.nodeInterpreter;
import interpreter.value;
import parser.ast;
import std.conv;
import interpreter.environment;
import interpreter.interpreter;

class NumberInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.NUMBER);
	}
	do {
		return Value(ValueType.NUMBER, to!string(ast.numberValue()));
	}

	/// Correctly interprets a numeric litteral
	unittest {
		auto env = new Environment();
		auto ast = new AST(NodeType.NUMBER, "4");
		auto inter = new NumberInterpreter();
		assert(inter.interpret(ast, env).numberValue == 4);
	}
}

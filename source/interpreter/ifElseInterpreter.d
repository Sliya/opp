module interpreter.ifElseInterpreter;

import interpreter.value;
import parser.ast;
import interpreter.nodeInterpreter;
import interpreter;
import interpreter.environment;

class IfElseInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.IF);
	}
	do {
		if (interpreter.interpret(ast.collection[0], env).boolValue) {
			return interpreter.interpret(ast.collection[1], env);
		}
		if (ast.collection[2]!is null) {
			return interpreter.interpret(ast.collection[2], env);
		}
		return Value.NIL;
	}

	/// It correctly interprets an if else expression
	unittest {
		auto env = new Environment();
		auto ifBloc = new AST(NodeType.NUMBER, "1");
		auto elseBloc = new AST(NodeType.NUMBER, "2");
		auto expr = new AST(NodeType.BOOLEAN, "true");
		auto ast = new AST(NodeType.IF, [expr, ifBloc, elseBloc]);
		auto inter = new IfElseInterpreter();
		auto value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER && value.numberValue == 1);
		expr = new AST(NodeType.BOOLEAN, "false");
		ast = new AST(NodeType.IF, [expr, ifBloc, elseBloc]);
		value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER && value.numberValue == 2);
	}

	/// It correctly interprets an if expression with no else
	unittest {
		auto env = new Environment();
		auto ifBloc = new AST(NodeType.NUMBER, "1");
		auto expr = new AST(NodeType.BOOLEAN, "true");
		auto ast = new AST(NodeType.IF, [expr, ifBloc, null]);
		auto inter = new IfElseInterpreter();
		auto value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER && value.numberValue == 1);
		expr = new AST(NodeType.BOOLEAN, "false");
		ast = new AST(NodeType.IF, [expr, ifBloc, null]);
		value = inter.interpret(ast, env);
		assert(value.type == ValueType.NIL);
	}
}

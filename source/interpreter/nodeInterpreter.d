module interpreter.nodeInterpreter;

import parser.ast;
import interpreter.value;
import interpreter.environment;
import interpreter.interpreter;
import std.format;
import std.algorithm;
import std.stdio;

/**
  * An interpreter in charge of a specific type of AST's node
  */
abstract class NodeInterpreter {

	protected Interpreter interpreter;

	this() {
		this(new Interpreter(makeGlobal!(StdFileHandle.stdout)));
	}

	this(Interpreter interpreter) {
		this.interpreter = interpreter;
	}

	/**
	  * Interprets the given AST in the given environment and returns the computed value
	  *
	  * Returns: the computed value of the AST
	  */
	Value interpret(AST ast, ref Environment env);

	static void checkType(Value v, ValueType expected) {
		if (v.type != expected) {
			throw new Exception(format("Runtime exception: expected %s, got %s", expected, v.type));
		}
	}

	static void checkType(Value v, ValueType[] expected) {
		if (!expected.canFind(v.type)) {
			throw new Exception(format("Runtime exception: expected %(%s or %), got %s", expected, v.type));
		}
	}

	/// checktype does not throw if the types match
	unittest {
		auto v = Value.number(4);
		checkType(v, ValueType.NUMBER);
	}

	/// checktype throws if the types does not match
	unittest {
		auto v = Value.number(4);
		try {
			checkType(v, ValueType.BOOLEAN);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// checktype does not throw if one of the type matches
	unittest {
		auto v = Value.number(4);
		checkType(v, [ValueType.BOOLEAN, ValueType.NUMBER]);
	}

	/// checktype throws if none of the type matches
	unittest {
		auto v = Value.number(4);
		try {
			checkType(v, [ValueType.BOOLEAN, ValueType.STRING]);
			assert(false);
		} catch (Exception e) {
		}
	}
}

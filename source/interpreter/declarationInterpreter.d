module interpreter.declarationInterpreter;
import interpreter.nodeInterpreter;
import parser.ast;
import interpreter.value;
import interpreter.environment;
import interpreter;

class DeclarationInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type = NodeType.DECLARATION);
	}
	do {
		auto value = interpreter.interpret(ast.right, env);
		auto id = ast.left.value;
		env.declare(id, value, ast.value == "let");
		return value;
	}

	/// correctly interprets a let assignment
	unittest {
		auto env = new Environment();
		auto value = new AST(NodeType.STRING, "\"hey\"");
		auto id = new AST(NodeType.IDENTIFIER, "a");
		auto assignment = new AST(NodeType.DECLARATION, "let", id, value);
		auto inter = new DeclarationInterpreter();
		assert(inter.interpret(assignment, env).stringValue == "hey");
		assert(env.get("a").stringValue == "hey");
	}

	/// correctly interprets a const assignment
	unittest {
		auto env = new Environment();
		auto value = new AST(NodeType.STRING, "\"hey\"");
		auto id = new AST(NodeType.IDENTIFIER, "a");
		auto assignment = new AST(NodeType.DECLARATION, "let mut", id, value);
		auto inter = new DeclarationInterpreter();
		assert(inter.interpret(assignment, env).stringValue == "hey");
	}

	/// it does not allow to declare a variable twice
	unittest {
		auto env = new Environment();
		auto value = new AST(NodeType.STRING, "\"hey\"");
		auto id = new AST(NodeType.IDENTIFIER, "a");
		auto assignment = new AST(NodeType.DECLARATION, "let ", id, value);
		auto inter = new DeclarationInterpreter();
		assert(inter.interpret(assignment, env).stringValue == "hey");
		try {
			inter.interpret(assignment, env);
			assert(false);
		} catch (Exception e) {

		}
	}
}

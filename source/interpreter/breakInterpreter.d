module interpreter.breakInterpreter;

import parser.ast;
import interpreter.value;
import interpreter;
import interpreter.nodeInterpreter;
import interpreter.environment;
import interpreter.breakException;

class BreakInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.BREAK);
	}
	do {
		throw new BreakException(interpreter.interpret(ast.left, env));
	}

	/// It correctly interprets a break
	unittest {
		auto expr = new AST(NodeType.NUMBER, "3");
		auto ast = new AST(NodeType.BREAK, "break", expr, null);
		auto env = new Environment();
		auto inter = new BreakInterpreter();
		try {
			inter.interpret(ast, env);
		} catch (BreakException r) {
			auto value = r.value;
			assert(value.type == ValueType.NUMBER);
		}
	}

	/// It correctly interprets a break
	unittest {
		auto expr = new AST(NodeType.ARRAY, [new AST(NodeType.NUMBER, "3")]);
		auto ast = new AST(NodeType.BREAK, "break", expr, null);
		auto env = new Environment();
		auto inter = new BreakInterpreter();
		try {
			inter.interpret(ast, env);
		} catch (BreakException r) {
			auto value = r.value;
			assert(value.type == ValueType.ARRAY);
			assert(value.values.length == 1);
		}
	}
}

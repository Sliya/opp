module interpreter.whileInterpreter;

import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.value;
import interpreter;
import interpreter.environment;
import interpreter.breakException;

class WhileInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env) {
		auto condition = ast.collection[0];
		auto block = ast.collection[1];
		auto ret = Value(ValueType.NIL, "NIL");
		try {
			while (interpreter.interpret(condition, env).boolValue) {
				ret = interpreter.interpret(block, env);
			}
		} catch (BreakException r) {
			return r.value;
		}
		return ret;
	}

	/// It correctly interprets a while loop
	unittest {
		auto env = new Environment();
		env.declare("i", Value(ValueType.NUMBER, "0"), false);
		auto left = new AST(NodeType.IDENTIFIER, "i");
		auto right = new AST(NodeType.NUMBER, "10");
		auto condition = new AST(NodeType.BINARY_OP, "<", left, right);
		auto inc = new AST(NodeType.BINARY_OP, "+", left, new AST(NodeType.NUMBER, "1"));
		auto block = new AST(NodeType.ASSIGNMENT, "=", left, inc);
		auto ast = new AST(NodeType.WHILE, [condition, block]);
		auto inter = new WhileInterpreter();
		auto value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER && value.numberValue == 10);
	}

	/// It correctly interprets a while loop
	unittest {
		auto env = new Environment();
		env.declare("i", Value(ValueType.NUMBER, "0"), false);
		auto left = new AST(NodeType.IDENTIFIER, "i");
		auto right = new AST(NodeType.NUMBER, "10");
		auto condition = new AST(NodeType.BINARY_OP, "<", left, right);
		auto block = new AST(NodeType.BREAK, "", left, null);
		auto ast = new AST(NodeType.WHILE, [condition, block]);
		auto inter = new WhileInterpreter();
		auto value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER && value.numberValue == 0);
	}
}

module interpreter.unaryOpInterpreter;

import std.conv : to;
import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.value;
import interpreter;
import interpreter.environment;

class UnaryOpInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type = NodeType.UNARY_OP);
	}
	do {
		auto left = interpreter.interpret(ast.left, env);
		switch (ast.value) {
		case "!":
			checkType(left, ValueType.BOOLEAN);
			return Value(ValueType.BOOLEAN, to!string(!left.boolValue));
		case "-":
			checkType(left, ValueType.NUMBER);
			return Value(ValueType.NUMBER, to!string(-left.numberValue()));
		case "++":
			checkType(left, ValueType.NUMBER);
			auto v = Value(ValueType.NUMBER, to!string(left.numberValue + 1));
			env.set(ast.left.value, v);
			return left;
		case "--":
			checkType(left, ValueType.NUMBER);
			auto v = Value(ValueType.NUMBER, to!string(left.numberValue - 1));
			env.set(ast.left.value, v);
			return left;
		default:
			throw new Exception("Unknown unary operator " ~ ast.value);
		}
	}

	// Correctly interprets a minus
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.NUMBER, "8");
		auto op = new AST(NodeType.UNARY_OP, "-", left, null);
		auto inter = new UnaryOpInterpreter();
		auto value = inter.interpret(op, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == -8);
	}

	// Correctly interprets a not
	unittest {
		auto env = new Environment();
		auto left = new AST(NodeType.BOOLEAN, "true");
		auto op = new AST(NodeType.UNARY_OP, "!", left, null);
		auto inter = new UnaryOpInterpreter();
		auto value = inter.interpret(op, env);
		assert(value.type == ValueType.BOOLEAN);
		assert(!value.boolValue);
	}

	// Correctly interprets an increment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value(ValueType.NUMBER, "1"), false);
		auto left = new AST(NodeType.IDENTIFIER, "foo");
		auto op = new AST(NodeType.UNARY_OP, "++", left, null);
		auto inter = new UnaryOpInterpreter();
		auto value = inter.interpret(op, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 1);
		assert(env.get("foo").numberValue == 2);
	}

	// Correctly interprets a decrement
	unittest {
		auto env = new Environment();
		env.declare("foo", Value(ValueType.NUMBER, "1"), false);
		auto left = new AST(NodeType.IDENTIFIER, "foo");
		auto op = new AST(NodeType.UNARY_OP, "--", left, null);
		auto inter = new UnaryOpInterpreter();
		auto value = inter.interpret(op, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 1);
		assert(env.get("foo").numberValue == 0);
	}

}

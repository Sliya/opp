module interpreter.arrayInterpreter;

import parser.ast;
import interpreter.value;
import interpreter.environment;
import interpreter.nodeInterpreter;
import std.algorithm;
import interpreter;
import std.array;

class ArrayInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override public Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.ARRAY);
	}
	do {
		auto values = ast.collection.map!(it => interpreter.interpret(it, env)).array;
		return Value.array(values);
	}

	/// It correctly interprets arrays
	unittest {
		auto env = new Environment();
		auto inter = new ArrayInterpreter();
		auto ast = new AST(NodeType.ARRAY,
				[new AST(NodeType.NUMBER, "1"),
				new AST(NodeType.NUMBER, "2"),
				new AST(NodeType.NUMBER, "3")]);
		auto v = inter.interpret(ast, env);
		assert(v.type == ValueType.ARRAY);
		assert(v.values.length == 3);
	}
}

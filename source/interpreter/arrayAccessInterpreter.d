module interpreter.arrayAccessInterpreter;

import parser.ast;
import interpreter.value;
import interpreter.environment;
import interpreter.nodeInterpreter;
import interpreter;
import std.conv : to;

class ArrayAccessInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override public Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.ARRAY_ACCESS);
	}
	do {
		auto array = interpreter.interpret(ast.left, env);
		switch (array.type) {
		case ValueType.ARRAY:
		case ValueType.STRING:
			auto idxValue = interpreter.interpret(ast.right, env);
			checkType(idxValue, ValueType.NUMBER);
			auto idx = to!int(idxValue.numberValue);
			return array.type == ValueType.ARRAY
				? array.values[idx] : Value.str(to!string(array.stringValue[idx]));
		case ValueType.OBJECT:
			auto idxValue = interpreter.interpret(ast.right, env);
			checkType(idxValue, ValueType.STRING);
			auto idx = idxValue.stringValue;
			return array.attributes[idx];
		default:
			import std.format;

			throw new Exception(format("Runtime exception: expected %(%s or %), got %s", [
						ValueType.OBJECT, ValueType.ARRAY, ValueType.STRING
					], array.type));
		}
	}

	/// It correctly interprets array access
	unittest {
		auto left = new AST(NodeType.ARRAY, [
				new AST(NodeType.NUMBER, "0"),
				new AST(NodeType.NUMBER, "1")
				]);
		auto right = new AST(NodeType.NUMBER, "1");
		auto ast = new AST(NodeType.ARRAY_ACCESS, "", left, right);
		auto inter = new ArrayAccessInterpreter();
		auto env = new Environment();
		auto v = inter.interpret(ast, env);
		assert(v.type == ValueType.NUMBER);
		assert(v.numberValue == 1);
	}

	/// It throws when accessing something else than an array, string or object
	unittest {
		auto left = new AST(NodeType.NUMBER, "0");
		auto right = new AST(NodeType.NUMBER, "1");
		auto ast = new AST(NodeType.ARRAY_ACCESS, "", left, right);
		auto inter = new ArrayAccessInterpreter();
		auto env = new Environment();
		try {
			auto v = inter.interpret(ast, env);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// It correctly interprets array access on strings
	unittest {
		auto left = new AST(NodeType.STRING, "\"bar\"");
		auto right = new AST(NodeType.NUMBER, "1");
		auto ast = new AST(NodeType.ARRAY_ACCESS, "", left, right);
		auto inter = new ArrayAccessInterpreter();
		auto env = new Environment();
		auto v = inter.interpret(ast, env);
		assert(v.type == ValueType.STRING);
		assert(v.stringValue == "a");
	}

	/// It correctly interprets array access on objects
	unittest {
		auto left = new AST(NodeType.OBJECT,
				[
				new AST(NodeType.IDENTIFIER, "attribute", new AST(NodeType.IDENTIFIER, "foo", null, null), new AST(
					NodeType.NUMBER, "1")),
				new AST(NodeType.IDENTIFIER, "attribute", new AST(NodeType.IDENTIFIER, "bar", null, null), new AST(
					NodeType.NUMBER, "2"))
				]);
		auto right = new AST(NodeType.STRING, `"bar"`);
		auto ast = new AST(NodeType.ARRAY_ACCESS, "", left, right);
		auto inter = new ArrayAccessInterpreter();
		auto env = new Environment();
		auto v = inter.interpret(ast, env);
		assert(v.type == ValueType.NUMBER);
		assert(v.numberValue == 2);
	}
}

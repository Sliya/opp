module interpreter.functionDeclarationInterpreter;

import parser.ast;
import interpreter.value;
import interpreter.nodeInterpreter;
import interpreter.environment;
import interpreter.interpreter;

class FunctionDeclarationInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.FUNCTION_DECLARATION);
	}
	do {
		auto fun = Value.fun(ast.collection, ast.right, env);
		if (ast.left !is null) {
			env.declare(ast.left.value, fun, true);
		}
		return fun;
	}

	/// It correctly interprets a function declaration
	unittest {
		auto env = new Environment();
		AST[] params;
		auto name = new AST(NodeType.IDENTIFIER, "pi");
		auto block = new AST(NodeType.NUMBER, "3.14");
		auto ast = new AST(NodeType.FUNCTION_DECLARATION, "fun", name, block, params);
		auto inter = new FunctionDeclarationInterpreter();
		inter.interpret(ast, env);
		auto fun = env.get("pi");
		assert(fun.type == ValueType.FUNCTION);
	}
}

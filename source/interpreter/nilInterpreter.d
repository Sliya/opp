module interpreter.nilInterpreter;

import interpreter.value;
import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.environment;
import interpreter.interpreter;

class NilInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override public Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.NIL);
	}
	do {
		return Value.NIL();
	}

	/// It correctly interprets nil
	unittest {
		auto ast = new AST(NodeType.NIL, "nil");
		auto inter = new NilInterpreter();
		auto env = new Environment();
		auto v = inter.interpret(ast, env);
		assert(v.type == ValueType.NIL);
	}

}

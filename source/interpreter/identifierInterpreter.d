module interpreter.identifierInterpreter;

import interpreter.value;
import interpreter.nodeInterpreter;
import parser.ast;
import interpreter.environment;
import interpreter.interpreter;

class IdentifierInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.IDENTIFIER);
	}
	do {
		return env.get(ast.value);
	}

	// It correctly interprets an identifier
	unittest {
		auto env = new Environment();
		env.declare("foo", Value(ValueType.STRING, "bar"), false);
		auto ast = new AST(NodeType.IDENTIFIER, "foo", null, null);
		auto inter = new IdentifierInterpreter();
		assert(inter.interpret(ast, env).stringValue == "bar");
	}
}

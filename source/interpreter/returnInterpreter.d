module interpreter.returnInterpreter;

import parser.ast;
import interpreter.value;
import interpreter;
import interpreter.nodeInterpreter;
import interpreter.environment;
import interpreter.returnException;

class ReturnInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.RETURN);
	}
	do {
		throw new ReturnException(interpreter.interpret(ast.left, env));
	}

	/// It correctly interprets a return
	unittest {
		auto expr = new AST(NodeType.NUMBER, "3");
		auto ast = new AST(NodeType.RETURN, "return", expr, null);
		auto env = new Environment();
		auto inter = new ReturnInterpreter();
		try {
			inter.interpret(ast, env);
		} catch (ReturnException r) {
			auto value = r.value;
			assert(value.type == ValueType.NUMBER);
		}
	}

	/// It correctly interprets a return
	unittest {
		auto expr = new AST(NodeType.ARRAY, [new AST(NodeType.NUMBER, "3")]);
		auto ast = new AST(NodeType.RETURN, "return", expr, null);
		auto env = new Environment();
		auto inter = new ReturnInterpreter();
		try {
			inter.interpret(ast, env);
		} catch (ReturnException r) {
			auto value = r.value;
			assert(value.type == ValueType.ARRAY);
			assert(value.values.length == 1);
		}
	}
}

module interpreter.stringInterpreter;

import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.value;
import interpreter.environment;
import interpreter.interpreter;

class StringInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.STRING);
	}
	do {
		return Value(ValueType.STRING, ast.stringValue);
	}

	/// Correctly interprets a string litteral
	unittest {
		auto env = new Environment();
		auto ast = new AST(NodeType.STRING, "\"hello\"");
		auto inter = new StringInterpreter();
		assert(inter.interpret(ast, env).stringValue == "hello");
	}
}

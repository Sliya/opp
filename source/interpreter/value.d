module interpreter.value;

import std.conv;
import parser.ast;
import interpreter.environment;
import std.format;
import object;
import std.algorithm;

enum ValueType {
	NUMBER,
	BOOLEAN,
	NIL,
	FUNCTION,
	ARRAY,
	OBJECT,
	STRING
}

struct Value {

	private string _value;
	private ValueType _type;
	private AST[] _functionParameters;
	private AST _functionBody;
	private Value[] _values;
	private Value[string] _attributes;
	private Environment _closure;

	this(ValueType type, string value) {
		this._type = type;
		this._value = value;
	}

	this(AST[] parameters, AST block, Environment closure) {
		this._type = ValueType.FUNCTION;
		this._functionParameters = parameters;
		this._functionBody = block;
		this._closure = closure;
	}

	this(Value[] values) {
		this._type = ValueType.ARRAY;
		this._values = values.dup;
		this._functionBody = null;
	}

	this(Value[string] attributes) {
		this._type = ValueType.OBJECT;
		this._attributes = attributes.dup;
		this._functionBody = null;
	}

	public static Value number(double value) {
		return Value(ValueType.NUMBER, to!string(value));
	}

	public static Value str(string value) {
		return Value(ValueType.STRING, value);
	}

	public static Value boolean(bool value) {
		return Value(ValueType.BOOLEAN, to!string(value));
	}

	public static Value fun(AST[] parameters, AST block, Environment closure) {
		return Value(parameters, block, closure);
	}

	public static Value array(Value[] values) {
		return Value(values);
	}

	public static Value object(Value[string] attributes) {
		return Value(attributes);
	}

	@property public ValueType type() {
		return _type;
	}

	@property public string value() {
		return _value;
	}

	@property public double numberValue() {
		return to!double(_value);
	}

	@property public string stringValue() {
		return _value;
	}

	@property public bool boolValue() {
		return _value == "true";
	}

	@property public AST[] parameters() {
		return _functionParameters;
	}

	@property public Value[] values() {
		return _values;
	}

	@property public Value[string] attributes() {
		return _attributes;
	}

	@property public AST body() {
		return _functionBody;
	}

	@property public static Value NIL() {
		return Value(ValueType.NIL, "nil");
	}

	@property public Environment closure() {
		return _closure;
	}

	public string toString() {
		final switch (_type) {
		case ValueType.STRING:
		case ValueType.NUMBER:
		case ValueType.BOOLEAN:
		case ValueType.NIL:
			return _value;
		case ValueType.OBJECT:
			auto values = _attributes.byKeyValue.map!(it => it.key ~ ": " ~ it.value.toString);
			return format("{%-(%s%|, %)}", values);
		case ValueType.ARRAY:
			auto values = _values.map!(it => it.toString);
			return format("[%-(%s%|, %)]", values);
		case ValueType.FUNCTION:
			auto params = _functionParameters.map!(it => it.value);
			return format("fun (%-(%s%|, %))", params);
		}
	}

	unittest {
		auto v = Value.NIL;
		assert("nil" == v.toString);
	}

	unittest {
		auto v = Value.number(8);
		assert("8" == v.toString);
	}

	unittest {
		auto v = Value.str("foo");
		assert("foo" == v.toString);
	}

	unittest {
		auto v = Value.boolean(true);
		assert("true" == v.toString);
	}

	unittest {
		auto v = Value.array([Value.number(1), Value.number(2)]);
		assert("[1, 2]" == v.toString);
	}

	unittest {
		auto v = Value.object(["foo": Value.number(1), "bar": Value.str("baz")]);
		assert(`{foo: 1, bar: baz}` == v.toString);
	}

	unittest {
		auto v = Value.fun([new AST(NodeType.IDENTIFIER, "bar")], null, null);
		assert("fun (bar)" == v.toString);
	}
}

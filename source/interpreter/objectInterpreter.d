module interpreter.objectInterpreter;

import interpreter.nodeInterpreter;
import interpreter.value;
import parser.ast;
import std.conv;
import std.typecons;
import std.algorithm;
import std.array;
import interpreter.environment;
import interpreter.interpreter;

class ObjectInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.OBJECT);
	}
	do {
		auto values = ast.collection.map!(it => tuple(it.left.value, interpreter.interpret(it.right, env))).assocArray;
		return Value.object(values);
	}

	/// Correctly interprets a numeric litteral
	unittest {
		auto env = new Environment();
		auto inter = new ObjectInterpreter();
		auto ast = new AST(NodeType.OBJECT,
				[
				new AST(NodeType.IDENTIFIER, "attribute", new AST(NodeType.IDENTIFIER, "foo", null, null), new AST(
					NodeType.NUMBER, "1")),
				new AST(NodeType.IDENTIFIER, "attribute", new AST(NodeType.IDENTIFIER, "bar", null, null), new AST(
					NodeType.NUMBER, "2"))
				]);
		auto v = inter.interpret(ast, env);
		assert(v.type == ValueType.OBJECT);
		assert(v.attributes.length == 2);
	}
}

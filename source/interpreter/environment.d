module interpreter.environment;

class Environment {
	import interpreter.value;
	import std.stdio;
	import std.format;

	private Environment parent;
	private Value[string] mem;
	private bool[string] immutability;

	this() {
	}

	this(Environment parent) {
		this.parent = parent;
	}

	public Value declare(string id, Value value, bool isImmutable) {
		if (id in mem) {
			throw new Exception("Runtime exception: variable " ~ id ~ " is already declared");
		}
		immutability[id] = isImmutable;
		mem[id] = value;
		return value;
	}

	public Value set(string id, Value value) {
		if (id in mem) {
			if (mem[id].type != value.type) {
				throw new Exception(format("Runtime exception: cannot store a %s in a %s variable", value.type, mem[id]
						.type));
			}
			if (immutability[id]) {
				throw new Exception("Runtime exception: cannot change the value of a constant");
			}
			mem[id] = value;
			return value;
		}
		if (parent !is null) {
			return parent.set(id, value);
		}
		mem[id] = value;
		return value;
	}

	public Value get(string id) {
		if (id in mem) {
			return mem[id];
		}
		if (parent !is null) {
			return parent.get(id);
		}
		throw new Exception("Runtime exception: unknown identifier " ~ id);
	}

	/// A variable cannot change type
	unittest {
		auto env = new Environment();
		auto numberValue = Value(ValueType.NUMBER, "10");
		auto stringValue = Value(ValueType.STRING, "\"hello\"");
		env.declare("foo", numberValue, false);
		assert(env.get("foo") == numberValue);
		try {
			env.set("foo", stringValue);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// A mutable variable can be changed
	unittest {
		auto env = new Environment();
		auto v = Value(ValueType.NUMBER, "10");
		env.declare("foo", v, false);
		assert(env.get("foo") == v);
		v = Value(ValueType.NUMBER, "11");
		env.set("foo", v);
		assert(env.get("foo") == v);
	}

	/// An immutable variable cannot be changed
	unittest {
		auto env = new Environment();
		auto v = Value(ValueType.NUMBER, "10");
		env.declare("foo", v, true);
		assert(env.get("foo") == v);
		v = Value(ValueType.NUMBER, "11");
		try {
			env.set("foo", v);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// Retrieves a variable from a parent environment
	unittest {
		auto parent = new Environment();
		auto v = Value(ValueType.NUMBER, "10");
		parent.declare("foo", v, true);
		auto env = new Environment(parent);
		assert(env.get("foo") == v);
	}

	/// Allows variable shadowing
	unittest {
		auto parent = new Environment();
		auto v = Value.number(10);
		auto v2 = Value.number(2);
		parent.declare("foo", v, true);
		auto env = new Environment(parent);
		env.declare("foo", v2, false);
		assert(env.get("foo") == v2);
	}
}

module interpreter.functionCallInterpreter;

import interpreter.value;
import parser.ast;
import interpreter.nodeInterpreter;
import interpreter.environment;
import std.format;
import std.stdio;
import interpreter;
import std.algorithm;
import interpreter.returnException;

class FunctionCallInterpreter : NodeInterpreter {

	private Value delegate(AST[], Environment)[string] builtIns;

	this() {
		super();
		addBuiltIns();
	}

	this(Interpreter inter) {
		super(inter);
		addBuiltIns();
	}

	private void addBuiltIns() {
		builtIns["print"] = &print;
		builtIns["println"] = &println;
		builtIns["to"] = &to;
		builtIns["length"] = &length;
		builtIns["read"] = &read;
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.FUNCTION_CALL);
	}
	do {
		if (ast.left.type == NodeType.FUNCTION_CALL) {
			auto fun = interpreter.interpret(ast.left, env);
			return call(fun, ast.collection, env);
		}
		if (ast.left.value in builtIns) {
			return builtIns[ast.left.value](ast.collection, env);
		}
		auto fun = interpreter.interpret(ast.left, env);
		return call(fun, ast.collection, env);
	}

	/// it correctly interprets a function call
	unittest {
		// Function declaration
		auto env = new Environment();
		auto block = new AST(NodeType.BINARY_OP, "*", new AST(NodeType.NUMBER, "2"), new AST(NodeType.IDENTIFIER, "n"));
		env.declare("double", Value.fun([new AST(NodeType.IDENTIFIER, "n")], block, null), true);

		// Function call
		auto params = [new AST(NodeType.NUMBER, "3")];
		auto ast = new AST(NodeType.FUNCTION_CALL, "", new AST(NodeType.IDENTIFIER, "double"), null, params);
		auto inter = new FunctionCallInterpreter();
		auto value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 6);
	}

	/// it correctly interprets a function call with a return
	unittest {
		// Function declaration
		auto env = new Environment();
		auto block = new AST(NodeType.RETURN, "", new AST(NodeType.NUMBER, "2"), null);
		env.declare("double", Value.fun([new AST(NodeType.IDENTIFIER, "n")], block, null), true);

		// Function call
		auto params = [new AST(NodeType.NUMBER, "3")];
		auto ast = new AST(NodeType.FUNCTION_CALL, "", new AST(NodeType.IDENTIFIER, "double"), null, params);
		auto inter = new FunctionCallInterpreter();
		auto value = inter.interpret(ast, env);
		assert(value.type == ValueType.NUMBER);
		assert(value.numberValue == 2);
	}

	/// it curries functions
	unittest {
		// Function declaration
		auto env = new Environment();
		auto block = new AST(NodeType.RETURN, "", new AST(NodeType.NUMBER, "2"), null);
		env.declare("double", Value.fun([new AST(NodeType.IDENTIFIER, "n")], block, null), true);

		// Function call
		AST[] params = [];
		auto ast = new AST(NodeType.FUNCTION_CALL, "", new AST(NodeType.IDENTIFIER, "double"), null, params);
		auto inter = new FunctionCallInterpreter();
		try {
			auto value = inter.interpret(ast, env);
			assert(value.type == ValueType.FUNCTION);
		} catch (Exception e) {
		}
	}

	private Value read(AST[] collection, Environment env) {
		auto str = readln();
		return Value.str(str[0 .. $ - 1]);
	}

	private Value length(AST[] collection, Environment env) {
		auto val = interpreter.interpret(collection[0], env);
		if (val.type == ValueType.ARRAY) {
			return Value.number(val.values.length);
		}
		if (val.type == ValueType.STRING) {
			return Value.number(val.stringValue().length);
		}
		throw new Exception(format("Runtime exception: length function expected an array or a string, got %s", val.type));
	}

	private Value print(AST[] collection, Environment env) {
		auto str = collection.map!(it => interpreter.interpret(it, env))
			.map!(it => it.toString())
			.reduce!((a, b) => a ~ b);
		interpreter.output.write(str);
		return Value.str(str);
	}

	private Value println(AST[] collection, Environment env) {
		auto str = collection.map!(it => interpreter.interpret(it, env))
			.map!(it => it.toString())
			.reduce!((a, b) => a ~ b);
		interpreter.output.writeln(str);
		return Value.str(str);
	}

	private Value to(AST[] collection, Environment env) {
		if (collection.length != 2 || !["string", "number", "boolean"].canFind(collection[0].stringValue)) {
			throw new Exception("Runtime exception: the function 'to' takes 2 arguments, the first one is a string representing the type (string, number, boolean), the second is the value to cast");
		}
		auto typeValue = collection[0].stringValue;
		auto valueToBeCast = interpreter.interpret(collection[1], env);
		final switch (typeValue) {
		case "string":
			return Value.str(valueToBeCast.stringValue);
		case "number":
			return Value.number(valueToBeCast.numberValue);
		case "boolean":
			return Value.boolean(valueToBeCast.boolValue);
		}
	}

	private Value call(Value fun, AST[] args, Environment env) {
		if (fun.parameters.length > args.length) {
			auto extendedEnv = new Environment(fun.closure);
			foreach (i, arg; args) {
				extendedEnv.declare(fun.parameters[i].value, interpreter.interpret(arg, env), true);
			}
			return Value.fun(fun.parameters[args.length .. $], fun.body, extendedEnv);
		}
		auto funEnv = new Environment(fun.closure);
		foreach (i, param; fun.parameters) {
			funEnv.declare(param.value, interpreter.interpret(args[i], env), true);
		}
		try {
			return interpreter.interpret(fun.body, funEnv);
		} catch (ReturnException r) {
			return r.value;
		}
	}

}

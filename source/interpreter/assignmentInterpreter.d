module interpreter.assignmentInterpreter;

import interpreter.nodeInterpreter;
import parser.ast;
import interpreter.value;
import interpreter.environment;
import interpreter;
import std.conv : to;
import std.format;
import std.algorithm;

class AssignmentInterpreter : NodeInterpreter {

	this() {
		super();
	}

	this(Interpreter inter) {
		super(inter);
	}

	override Value interpret(AST ast, ref Environment env)
	in {
		assert(ast.type == NodeType.ASSIGNMENT);
	}
	do {
		auto right = interpreter.interpret(ast.right, env);
		if (ast.left.type == NodeType.IDENTIFIER) {
			auto id = ast.left.value;
			switch (ast.value) {
			case "=":
				return env.set(id, right);
			case "+=":
				return env.set(id, Value.number(env.get(id).numberValue + right.numberValue));
			case "-=":
				return env.set(id, Value.number(env.get(id).numberValue - right.numberValue));
			case "*=":
				return env.set(id, Value.number(env.get(id).numberValue * right.numberValue));
			case "/=":
				return env.set(id, Value.number(env.get(id).numberValue / right.numberValue));
			case "%=":
				return env.set(id, Value.number(env.get(id).numberValue % right.numberValue));
			default:
				throw new Exception("Runtime exception: unknown assignment operator: " ~ ast.value);
			}
		} else if ([NodeType.ARRAY_ACCESS, NodeType.FUNCTION_CALL].canFind(ast.left.type)) {
			auto array = interpreter.interpret(ast.left.left, env);
			switch (array.type) {
			case ValueType.ARRAY:
				auto idx = interpreter.interpret(ast.left.right, env);
				checkType(idx, ValueType.NUMBER);
				array.values[to!int(idx.numberValue)] = right;
				return right;
			case ValueType.OBJECT:
				auto idx = interpreter.interpret(ast.left.right, env);
				checkType(idx, ValueType.STRING);
				array.attributes[idx.stringValue] = right;
				return right;
			default:
				throw new Exception(format("Runtime exception: expected %(%s or %), got %s", [
							ValueType.OBJECT, ValueType.ARRAY
						], array.type));
			}
		}
		throw new Exception(format("Runtime exception: expected %(%s or %), got %s", [
					NodeType.ARRAY_ACCESS, NodeType.FUNCTION_CALL, NodeType.IDENTIFIER
				], ast.left.type));
	}

	/// Correctly interprets an assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(0.5), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "1.5");
		auto ast = new AST(NodeType.ASSIGNMENT, "=", id, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").numberValue == 1.5);
	}

	/// It throws on unknown assignment operator
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(0.5), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "1.5");
		auto ast = new AST(NodeType.ASSIGNMENT, "µ=", id, right);
		auto inter = new AssignmentInterpreter();
		try {
			inter.interpret(ast, env);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// It throws on invalid lhs
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(0.5), false);
		auto id = new AST(NodeType.STRING, "foo");
		auto right = new AST(NodeType.NUMBER, "1.5");
		auto ast = new AST(NodeType.ASSIGNMENT, "=", id, right);
		auto inter = new AssignmentInterpreter();
		try {
			inter.interpret(ast, env);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// It throws on invalid array access lhs
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.array([Value.number(-1)]), true);
		assert(env.get("foo").values[0].numberValue == -1);
		auto id = new AST(NodeType.STRING, "foo");
		auto right = new AST(NodeType.NUMBER, "0");
		auto left = new AST(NodeType.ARRAY_ACCESS, "", id, right);
		auto ast = new AST(NodeType.ASSIGNMENT, "=", left, right);
		auto inter = new AssignmentInterpreter();
		try {
			inter.interpret(ast, env);
			assert(false);
		} catch (Exception e) {
		}
	}

	/// Correctly interprets an array element assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.array([Value.number(-1)]), true);
		assert(env.get("foo").values[0].numberValue == -1);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "0");
		auto left = new AST(NodeType.ARRAY_ACCESS, "", id, right);
		auto ast = new AST(NodeType.ASSIGNMENT, "=", left, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").values[0].numberValue == 0);
	}

	/// Correctly interprets an object element assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.object(["bar": Value.number(-1)]), true);
		assert(env.get("foo").attributes["bar"].numberValue == -1);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto attr = new AST(NodeType.STRING, `"bar"`);
		auto left = new AST(NodeType.ARRAY_ACCESS, "", id, attr);
		auto right = new AST(NodeType.NUMBER, "0");
		auto ast = new AST(NodeType.ASSIGNMENT, "=", left, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").attributes["bar"].numberValue == 0);
	}

	/// Correctly interprets a += assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(1), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "1");
		auto ast = new AST(NodeType.ASSIGNMENT, "+=", id, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").numberValue == 2);
	}

	/// Correctly interprets a -= assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(2), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "1");
		auto ast = new AST(NodeType.ASSIGNMENT, "-=", id, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").numberValue == 1);
	}

	/// Correctly interprets a *= assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(2), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "3");
		auto ast = new AST(NodeType.ASSIGNMENT, "*=", id, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").numberValue == 6);
	}

	/// Correctly interprets a /= assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(8), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "4");
		auto ast = new AST(NodeType.ASSIGNMENT, "/=", id, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").numberValue == 2);
	}

	/// Correctly interprets a %= assignment
	unittest {
		auto env = new Environment();
		env.declare("foo", Value.number(12), false);
		auto id = new AST(NodeType.IDENTIFIER, "foo");
		auto right = new AST(NodeType.NUMBER, "10");
		auto ast = new AST(NodeType.ASSIGNMENT, "%=", id, right);
		auto inter = new AssignmentInterpreter();
		inter.interpret(ast, env);
		assert(env.get("foo").numberValue == 2);
	}
}

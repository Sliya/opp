import std.stdio;
import tokenizer;
import parser;
import std.getopt;
import std.file;
import interpreter;
import error.errorHandler;
import interpreter.environment;

bool verbose;
File output;

void main(string[] args) {
	bool replMode;
	string outStr = "stdio";
	getopt(args, "repl", &replMode,
			"out", &outStr,
			"verbose", &verbose);
	output = outStr == "stdio" ? makeGlobal!(StdFileHandle.stdout) : File(outStr, "w");
	if (replMode) {
		repl();
	} else {
		run(args[1]);
	}
}

void repl() {
	writeln("Welcome to the opp REPL. To quit, type 'q'");
	string line;
	auto env = new Environment();
	auto parser = new Parser();
	auto interpreter = new Interpreter(output);
	while (true) {
		output.write("% ");
		line = readln();
		if (line == "q\n") {
			break;
		}
		try {
			auto ast = parser.parse(line);
			if (ErrorHandler.hasErrors()) {
				ErrorHandler.logAndReset(output);
				continue;
			}
			if (ast !is null) {
				output.writeln(interpreter.interpret(ast, env).toString());
			}
		} catch (Throwable e) {
			output.writeln(e.msg);
		}
	}
}

void run(string filename) {
	auto parser = new Parser();
	auto interpreter = new Interpreter(output);
	auto env = new Environment();

	auto source = readText(filename);
	if (verbose) {
		output.writeln("Parsing...");
	}
	auto ast = parser.parse(source);
	if (verbose) {
		output.writeln("Parsing done!");
	}
	if (ErrorHandler.hasErrors()) {
		ErrorHandler.log(output);
		return;
	}
	if (verbose) {
		output.writeln("Interpreting...");
	}
	interpreter.interpret(ast, env);
}

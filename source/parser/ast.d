module parser.ast;

import std.variant;
import std.conv;
import vibe.data.json;

enum NodeType {
	BLOCK,
	NUMBER,
	BOOLEAN,
	NIL,
	BINARY_OP,
	UNARY_OP,
	WHILE,
	IF,
	ASSIGNMENT,
	DECLARATION,
	FUNCTION_DECLARATION,
	FUNCTION_CALL,
	DOT_CALL,
	IDENTIFIER,
	RETURN,
	BREAK,
	ARRAY,
	ARRAY_ACCESS,
	OBJECT,
	STRING
}

class AST {
	private NodeType _type;
	private AST _left;
	private AST _right;
	private AST[] _collection;
	private string _value;

	this(NodeType type, string value, AST left, AST right) {
		this._type = type;
		this._left = left;
		this._right = right;
		this._value = value;
	}

	this(NodeType type, string value, AST left, AST right, AST[] collection) {
		this._type = type;
		this._left = left;
		this._right = right;
		this._value = value;
		this._collection = collection;
	}

	this(NodeType type, string litteral) {
		this._type = type;
		this._left = null;
		this._right = null;
		this._value = litteral;
	}

	this(NodeType type, AST[] collection) {
		this._type = type;
		this._left = null;
		this._right = null;
		this._collection = collection;
	}

	this(NodeType type, string value, AST[] collection) {
		this._type = type;
		this._left = null;
		this._right = null;
		this._value = value;
		this._collection = collection;
	}

	@property NodeType type() {
		return _type;
	}

	@property AST left() {
		return _left;
	}

	@property AST right() {
		return _right;
	}

	@property AST[] collection() {
		return _collection;
	}

	@property void collection(AST[] collection) {
		_collection = collection;
	}

	@property string value() {
		return _value;
	}

	public double numberValue() {
		return to!double(_value);
	}

	public string stringValue() {
		return _value[1 .. $ - 1];
	}

	public bool boolValue() {
		return _value == "true";
	}

	override string toString() {
		return this.serializeToJson().toPrettyString();
	}
}

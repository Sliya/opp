module parser.parser;

class Parser {
	import tokenizer;
	import tokenizer.token;
	import parser.ast;
	import std.format;
	import error.errorHandler;
	import std.algorithm : canFind;

	private Token lookAhead;
	private Tokenizer lexer;

	/**
	 * Parses a string into an AST
	 */
	public AST parse(string code) {
		lexer = new Tokenizer(code);
		lookAhead = lexer.getNextToken();
		return Program();
	}

	/// It correctly parses a single expression
	unittest {
		auto parser = new Parser();
		auto source = "12";
		auto ast = parser.parse(source);
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 1);
		assert(ast.collection[0].type == NodeType.NUMBER);
		assert(ast.collection[0].numberValue == 12);
	}

	/// It correctly parses an expression terminated by a semicolon
	unittest {
		auto parser = new Parser();
		auto source = "12;";
		auto ast = parser.parse(source);
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 1);
		assert(ast.collection[0].type == NodeType.NUMBER);
		assert(ast.collection[0].numberValue == 12);
	}

	/// It correctly parses several instructions
	unittest {
		auto parser = new Parser();
		auto source = "
			// A number litteral
			12

			/*
			 * A string
		     */
			\"azerty\"
		";
		auto ast = parser.parse(source);
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 2);
		assert(ast.collection[0].type == NodeType.NUMBER);
		assert(ast.collection[0].numberValue == 12);
		assert(ast.collection[1].type == NodeType.STRING);
		assert(ast.collection[1].stringValue == "azerty");
	}

	/// It correctly parses an additive expression
	unittest {
		auto parser = new Parser();
		auto source = "3+4-1";
		auto ast = parser.parse(source);
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 1);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		auto left = ast.collection[0].left;
		auto right = ast.collection[0].right;
		assert(left.type == NodeType.BINARY_OP);
		assert(right.type == NodeType.NUMBER);
	}

	/// It correctly parses an additive boolean expression
	unittest {
		auto parser = new Parser();
		auto source = "a || false";
		auto ast = parser.parse(source);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		auto node = ast.collection[0];
		auto left = node.left;
		auto right = node.right;
		assert(node.value == "||");
		assert(left.type == NodeType.IDENTIFIER);
		assert(right.type == NodeType.BOOLEAN);
	}

	/// It correctly parses a multiplicative boolean expression
	unittest {
		auto parser = new Parser();
		auto source = "a && false";
		auto ast = parser.parse(source);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		auto node = ast.collection[0];
		auto left = node.left;
		auto right = node.right;
		assert(node.value == "&&");
		assert(left.type == NodeType.IDENTIFIER);
		assert(right.type == NodeType.BOOLEAN);
		source = "a < false";
		ast = parser.parse(source);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		node = ast.collection[0];
		left = node.left;
		right = node.right;
		assert(node.value == "<");
		assert(left.type == NodeType.IDENTIFIER);
		assert(right.type == NodeType.BOOLEAN);
	}

	/// It correctly parses a multiplicative expression
	unittest {
		auto parser = new Parser();
		auto source = "3+4*2";
		auto ast = parser.parse(source);
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 1);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		auto left = ast.collection[0].left;
		auto right = ast.collection[0].right;
		assert(left.type == NodeType.NUMBER);
		assert(right.type == NodeType.BINARY_OP);
	}

	/// It correctly parses an expression with parenthesis
	unittest {
		auto parser = new Parser();
		auto source = "3*(2+4)";
		auto ast = parser.parse(source);
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 1);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		assert(ast.collection[0].value == "*");
		auto left = ast.collection[0].left;
		auto right = ast.collection[0].right;
		assert(left.type == NodeType.NUMBER);
		assert(right.type == NodeType.BINARY_OP);
	}

	/// It correctly parses a variable declaration
	unittest {
		auto parser = new Parser();
		auto source = "let mut a = 8";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.DECLARATION);
		assert(ast.value == "let mut");
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.left.value == "a");
		assert(ast.right.type == NodeType.NUMBER);
		assert(ast.right.numberValue == 8);
		source = "let mut a = \"yo\"";
		ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.DECLARATION);
		assert(ast.value == "let mut");
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.left.value == "a");
		assert(ast.right.type == NodeType.STRING);
		assert(ast.right.stringValue == "yo");
	}

	/// It correctly parses a function declaration
	unittest {
		auto parser = new Parser();
		auto source = "fun add(a, b){a+b}";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_DECLARATION);
		assert(ast.value == "fun");
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.left.value == "add");
		assert(ast.right.type == NodeType.BLOCK);
		assert(ast.collection.length == 2);
		assert(ast.collection[0].type == NodeType.IDENTIFIER);
		assert(ast.collection[0].value == "a");
		assert(ast.collection[1].type == NodeType.IDENTIFIER);
		assert(ast.collection[1].value == "b");
	}

	/// It correctly parses an anonymous function
	unittest {
		auto parser = new Parser();
		auto source = "fun (a, b){a+b}";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_DECLARATION);
		assert(ast.value == "fun");
		assert(ast.left is null);
		assert(ast.right.type == NodeType.BLOCK);
		assert(ast.collection.length == 2);
		assert(ast.collection[0].type == NodeType.IDENTIFIER);
		assert(ast.collection[0].value == "a");
		assert(ast.collection[1].type == NodeType.IDENTIFIER);
		assert(ast.collection[1].value == "b");
	}

	/// It correctly parses a function call with no parameters
	unittest {
		auto parser = new Parser();
		auto source = "foo()";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_CALL);
		assert(ast.left.value == "foo");
		assert(ast.collection.length == 0);
	}

	/// It correctly parses a function call call with no parameters
	unittest {
		auto parser = new Parser();
		auto source = "foo()()";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_CALL);
		assert(ast.left.type == NodeType.FUNCTION_CALL);
		assert(ast.collection.length == 0);
	}

	/// It correctly parses a function call
	unittest {
		auto parser = new Parser();
		auto source = "add(pi, 7+3)";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_CALL);
		assert(ast.left.value == "add");
		assert(ast.collection.length == 2);
		assert(ast.collection[0].type == NodeType.IDENTIFIER);
		assert(ast.collection[0].value == "pi");
		assert(ast.collection[1].type == NodeType.BINARY_OP);
	}

	/// It correctly parses a declaration
	unittest {
		auto parser = new Parser();
		auto source = "let a = 8";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.DECLARATION);
		assert(ast.value == "let");
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.left.value == "a");
		assert(ast.right.type == NodeType.NUMBER);
		assert(ast.right.numberValue == 8);
	}

	/// It correctly parses a reassignment
	unittest {
		auto parser = new Parser();
		auto source = "a = 8";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ASSIGNMENT);
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.left.value == "a");
		assert(ast.right.type == NodeType.NUMBER);
		assert(ast.right.numberValue == 8);
	}

	/// It correclty parses an identifier
	unittest {
		auto parser = new Parser();
		auto source = "a";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.IDENTIFIER);
		assert(ast.value == "a");
	}

	/// It correclty parses a boolean litteral
	unittest {
		auto parser = new Parser();
		auto source = "true";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BOOLEAN);
		assert(ast.boolValue);
		source = "false";
		ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BOOLEAN);
		assert(!ast.boolValue);
	}

	/// It correclty parses a math expression using an identifier
	unittest {
		auto parser = new Parser();
		auto source = "3+a";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BINARY_OP);
		assert(ast.left.type == NodeType.NUMBER);
		assert(ast.right.type == NodeType.IDENTIFIER);
		source = "a+3";
		ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BINARY_OP);
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.right.type == NodeType.NUMBER);
		source = "a*3";
		ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BINARY_OP);
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.right.type == NodeType.NUMBER);
	}

	/// It correclty parses a unary operation
	unittest {
		auto parser = new Parser();
		auto source = "!true";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.UNARY_OP);
		assert(ast.value == "!");
		assert(ast.left.type == NodeType.BOOLEAN);
		source = "-8";
		ast = parser.parse(source).collection[0];
		assert(ast.value == "-");
		assert(ast.type == NodeType.UNARY_OP);
		assert(ast.left.type == NodeType.NUMBER);
		source = "a++";
		ast = parser.parse(source).collection[0];
		assert(ast.value == "++");
		assert(ast.type == NodeType.UNARY_OP);
		assert(ast.left.type == NodeType.IDENTIFIER);
		source = "foo--";
		ast = parser.parse(source).collection[0];
		assert(ast.value == "--");
		assert(ast.type == NodeType.UNARY_OP);
		assert(ast.left.type == NodeType.IDENTIFIER);
	}

	/// It correctly parses a return with value
	unittest {
		auto parser = new Parser();
		auto source = "return 3*6";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.RETURN);
		assert(ast.left.type == NodeType.BINARY_OP);
	}

	/// It correctly parses a return
	unittest {
		auto parser = new Parser();
		auto source = "return;";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.RETURN);
		assert(ast.left.type == NodeType.NIL);
	}

	/// It correctly parses a break
	unittest {
		auto parser = new Parser();
		auto source = "break;";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BREAK);
	}

	/// It correctly parses a break with value
	unittest {
		auto parser = new Parser();
		auto source = "break 3*6";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BREAK);
		assert(ast.left.type == NodeType.BINARY_OP);
	}

	/// It correctly parses an if expression
	unittest {
		auto parser = new Parser();
		auto source = "if(1==1){4}";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.IF);
		assert(ast.collection.length == 3);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		assert(ast.collection[1].collection[0].type == NodeType.NUMBER);
		assert(ast.collection[2] is null);
	}

	/// It correctly parses an if expression with a single expression block
	unittest {
		auto parser = new Parser();
		auto source = "if(1==1) 4";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.IF);
		assert(ast.collection.length == 3);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		assert(ast.collection[1].type == NodeType.NUMBER);
		assert(ast.collection[2] is null);
	}

	/// It correctly parses an if else expression
	unittest {
		auto parser = new Parser();
		auto source = "if(1==1){4}else{5;6}";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.IF);
		assert(ast.collection.length == 3);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		assert(ast.collection[1].collection[0].type == NodeType.NUMBER);
		assert(ast.collection[2].collection[0].type == NodeType.NUMBER);
	}

	/// It correctly parses a while expression
	unittest {
		auto parser = new Parser();
		auto source = "while(1==1){4}";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.WHILE);
		assert(ast.collection.length == 2);
		assert(ast.collection[0].type == NodeType.BINARY_OP);
		assert(ast.collection[1].collection[0].type == NodeType.NUMBER);
	}

	/// It correctly parses foreach
	unittest {
		auto parser = new Parser();
		auto source = "foreach(elt, idx of array){a+b}";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.BLOCK);
		assert(ast.collection.length == 2);
		assert(ast.collection[0].type == NodeType.DECLARATION);
		assert(ast.collection[0].left.type == NodeType.IDENTIFIER);
		assert(ast.collection[0].left.value == "idx");
		assert(ast.collection[1].type == NodeType.WHILE);
	}

	/// It correctly parses nil
	unittest {
		auto parser = new Parser();
		auto source = "nil";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.NIL);
		assert(ast.value == "nil");
	}

	/// It correctly parses an empty array
	unittest {
		auto parser = new Parser();
		auto source = "[]";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ARRAY);
		assert(ast.collection.length == 0);
	}

	/// It correctly parses an array
	unittest {
		auto parser = new Parser();
		auto source = "[1,2,3]";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ARRAY);
		assert(ast.collection.length == 3);
	}

	/// It correctly parses an array access
	unittest {
		auto parser = new Parser();
		auto source = "foo[1]";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ARRAY_ACCESS);
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.right.type == NodeType.NUMBER);
	}

	/// It correctly parses an array access access
	unittest {
		auto parser = new Parser();
		auto source = "foo[1][2]";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ARRAY_ACCESS);
		assert(ast.left.type == NodeType.ARRAY_ACCESS);
		assert(ast.right.type == NodeType.NUMBER);
	}

	/// It correctly parses an array access
	unittest {
		auto parser = new Parser();
		auto source = "foo[i]";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ARRAY_ACCESS);
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.right.type == NodeType.IDENTIFIER);
	}

	/// It correctly parses an array access
	unittest {
		auto parser = new Parser();
		auto source = "foo()[i]";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.ARRAY_ACCESS);
		assert(ast.left.type == NodeType.FUNCTION_CALL);
		assert(ast.right.type == NodeType.IDENTIFIER);
	}

	/// It correctly parses UFCS
	unittest {
		auto parser = new Parser();
		auto source = "[i].foo(3)";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_CALL);
		assert(ast.left.type == NodeType.IDENTIFIER);
		assert(ast.collection.length == 2);
	}

	///It correctly parses chains of function call or array access
	unittest {
		auto parser = new Parser();
		auto source = "foo()[i]()[0]()[1]()";
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.FUNCTION_CALL);
		assert(ast.left.type == NodeType.ARRAY_ACCESS);
	}

	/// It returns null when encoutering a syntax error near the end
	unittest {
		auto parser = new Parser();
		auto source = "println(tt tt)";
		const ast = parser.parse(source).collection[0];
		assert(ast is null);
	}

	/// It correctly parses an object
	unittest {
		auto parser = new Parser();
		auto source = `{a:"foo", b:1}`;
		auto ast = parser.parse(source).collection[0];
		assert(ast.type == NodeType.OBJECT);
	}

	/**
	 * Entry point
	 *
	 * Program
	 *   : {Expression [SEPARATOR]}
	 *   ;
	 */
	private AST Program() {
		AST[] collection = [];
		while (lookAhead.type != TokenType.EOF) {
			collection ~= Expression();
			if (lookAhead.type == TokenType.SEPARATOR) {
				eat(TokenType.SEPARATOR);
			}
		}
		return new AST(NodeType.BLOCK, "program", collection);
	}

	/**
	 * Block
	 *   : OPEN_BRACE {Expression [SEPARATOR]} CLOSED_BRACE
	 *   ;
	 */
	private AST Block() {
		AST[] collection = [];
		if (lookAhead.type != TokenType.OPEN_BRACE) {
			return Expression();
		}
		eat(TokenType.OPEN_BRACE);
		while (lookAhead.type != TokenType.CLOSED_BRACE) {
			collection ~= Expression();
			if (lookAhead.type == TokenType.SEPARATOR) {
				eat(TokenType.SEPARATOR);
			}
		}
		eat(TokenType.CLOSED_BRACE);
		return new AST(NodeType.BLOCK, "block", collection);
	}

	/**
	 * Expression
	 *   : IfExpr
	 *   | WhileExpr
	 *   | Foreach
	 *   | Declaration
	 *   | Return
	 *   | Break
	 *   | Assignment
	 *   ;
	 */
	private AST Expression() {
		if (lookAhead.type == TokenType.EOF) {
			return null;
		}
		try {
			switch (lookAhead.type) {
			case TokenType.IF:
				return IfExpr();
			case TokenType.WHILE:
				return WhileExpr();
			case TokenType.FOREACH:
				return Foreach();
			case TokenType.VARIABLE_DECL:
				return Declaration();
			case TokenType.RETURN:
				return Return();
			case TokenType.BREAK:
				return Break();
			default:
				return Assignment();
			}
		} catch (Exception e) {
			return Expression();
		}
	}

	/**
	  * FunctionDeclaration
	  *   : FUNCTION_DECL [Identifier] OPEN_PAR [Identifier {COMMA Identifier}] CLOSED_PAR Block
	  *   ;
	  */
	private AST FunctionDeclaration() {
		auto fun = eat(TokenType.FUNCTION_DECL);
		AST[] parameters;
		auto name = lookAhead.type == TokenType.IDENTIFIER
			? Identifier() //
			 : null;
		eat(TokenType.OPEN_PAR);
		if (lookAhead.type == TokenType.IDENTIFIER) {
			parameters ~= Identifier();
			while (lookAhead.type == TokenType.COMMA) {
				eat(TokenType.COMMA);
				parameters ~= Identifier();
			}
		}
		eat(TokenType.CLOSED_PAR);
		AST block = Block();
		return new AST(NodeType.FUNCTION_DECLARATION, "fun", name, block, parameters);
	}

	/**
	  * Return
	  *   : RETURN Expression
	  *   ;
	  */
	private AST Return() {
		return breakOrReturn(TokenType.RETURN);
	}

	/**
	  * Break
	  *   : BREAK
	  *   ;
	  */
	private AST Break() {
		return breakOrReturn(TokenType.BREAK);
	}

	private AST breakOrReturn(TokenType type) {
		auto nodeType = type == TokenType.BREAK ? NodeType.BREAK : NodeType.RETURN;
		eat(type);
		if ([TokenType.SEPARATOR, TokenType.CLOSED_BRACE].canFind(lookAhead.type)) {
			return new AST(nodeType, "", new AST(NodeType.NIL, "nil"), null);
		}
		return new AST(nodeType, "", Expression(), null);
	}

	/** Assignment
	  *   : And [ASSIGN_OP And]
	  *   ;
	  */
	private AST Assignment() {
		auto left = And();
		if (lookAhead.type != TokenType.ASSIGN_OP) {
			return left;
		}
		auto token = eat(TokenType.ASSIGN_OP);
		auto right = And();
		return new AST(NodeType.ASSIGNMENT, token.value, left, right);
	}

	/** And
	  *   : Or {AND Or}
	  *   ;
	  */
	private AST And() {
		auto legalTokens = [TokenType.AND];
		return BinaryOp(legalTokens, &Or);
	}

	/** Or
	  *   : Equality {OR Equality}
	  *   ;
	  */
	private AST Or() {
		auto legalTokens = [TokenType.OR];
		return BinaryOp(legalTokens, &Equality);
	}

	/**
	  * Equality
	  *   : Comparison { (EQUAL | NOT_EQUAL) Comparison}
	  *   ;
	  */
	private AST Equality() {
		auto legalTokens = [TokenType.EQUAL, TokenType.NOT_EQUAL];
		return BinaryOp(legalTokens, &Comparison);
	}

	/**
	  * Comparison
	  *   : Term { (GREATER | LESS | GREATER_EQ | LESS_EQ) Term}
	  *   ;
	  */
	private AST Comparison() {
		auto legalTokens = [TokenType.GREATER, TokenType.LESS, TokenType.GREATER_EQ, TokenType.LESS_EQ];
		return BinaryOp(legalTokens, &Term);
	}

	/**
	  * Foreach
	  *   : FOREACH OPEN_PAR Identifier [COMMA Identifier] OF Identifier CLOSED_PAR Block
	  *   ;
	  */
	private AST Foreach() {
		eat(TokenType.FOREACH);
		eat(TokenType.OPEN_PAR);
		auto eltId = Identifier();
		eat(TokenType.COMMA);
		auto idxId = Identifier();
		eat(TokenType.OF);
		auto arrayId = Identifier();
		eat(TokenType.CLOSED_PAR);
		auto block = Block();
		auto eltDecl = new AST(NodeType.DECLARATION, "let mut", eltId, new AST(NodeType.ARRAY_ACCESS, "", arrayId, idxId));
		block.collection = eltDecl ~ block.collection;
		block.collection = block.collection ~ new AST(NodeType.UNARY_OP, "++", idxId, null);
		auto arrayLength = new AST(NodeType.FUNCTION_CALL, "", new AST(NodeType.IDENTIFIER, "length"), null, [
				arrayId
				]);
		auto expr = new AST(NodeType.BINARY_OP, "<", idxId, arrayLength);
		auto whileExpr = new AST(NodeType.WHILE, [expr, block]);
		auto idxDecl = new AST(NodeType.DECLARATION, "let mut", idxId, new AST(NodeType.NUMBER, "0"));
		return new AST(NodeType.BLOCK, "block", [idxDecl, whileExpr]);
	}

	/**
	  * WhileExpr
	  *   : WHILE OPEN_PAR Expression CLOSED_PAR Block
	  *   ;
	  */
	private AST WhileExpr() {
		eat(TokenType.WHILE);
		eat(TokenType.OPEN_PAR);
		auto expr = Expression();
		eat(TokenType.CLOSED_PAR);
		auto block = Block();
		return new AST(NodeType.WHILE, [expr, block]);
	}

	/**
	  * IfExpr
	  *   : IF OPEN_PAR Expression CLOSED_PAR Block [ELSE Bloc]
	  *   ;
	  */
	private AST IfExpr() {
		eat(TokenType.IF);
		eat(TokenType.OPEN_PAR);
		auto expr = Expression();
		eat(TokenType.CLOSED_PAR);
		auto ifBlock = Block();
		AST elseBlock = null;
		if (lookAhead.type == TokenType.ELSE) {
			eat(TokenType.ELSE);
			elseBlock = Block();
		}
		return new AST(NodeType.IF, [expr, ifBlock, elseBlock]);
	}

	/**
	  * Declaration
	  *   : VARIABLE_DECL Identifier ASSIGN_OP Expression
	  *   ;
	  */
	private AST Declaration() {
		eat(TokenType.VARIABLE_DECL);
		auto const declType = lookAhead.type == TokenType.MUT ? "let mut" : "let";
		if (declType == "let mut") {
			eat(TokenType.MUT);
		}
		auto id = Identifier();
		eat(TokenType.ASSIGN_OP);
		auto value = Expression();
		return new AST(NodeType.DECLARATION, declType, id, value);
	}

	/**
	  * Term
	  *   : Factor {(PLUS | MINUS ) Factor}
	  *   ;
	  */
	private AST Term() {
		auto legalTokens = [TokenType.PLUS, TokenType.MINUS];
		return BinaryOp(legalTokens, &Factor);
	}

	/**
	  * Factor
	  *   : Unary { (DIV | MULT | MOD) Unary}
	  *   ;
	  */
	private AST Factor() {
		auto legalTokens = [TokenType.DIV, TokenType.MULT, TokenType.MOD];
		return BinaryOp(legalTokens, &Unary);
	}

	/**
	  * Parses an expression of the form :
	  *   op {legalToken op}
	  * where legalToken is one of the tokens of legalTokens
	  */
	private AST BinaryOp(TokenType[] legalTokens, AST delegate() op) {
		auto left = op();
		if (!legalTokens.canFind(lookAhead.type)) {
			return left;
		}
		auto token = eat(lookAhead.type);
		auto right = op();
		auto ret = new AST(NodeType.BINARY_OP, token.value, left, right);
		while (legalTokens.canFind(lookAhead.type)) {
			token = eat(lookAhead.type);
			right = op();
			ret = new AST(NodeType.BINARY_OP, token.value, ret, right);
		}
		return ret;

	}

	/**
	  * Unary
	  *   : (NOT | MINUS) Unary
	  *   | Call
	  *   ;
	  */
	private AST Unary() {
		if (lookAhead.type == TokenType.NOT || lookAhead.type == TokenType.MINUS) {
			auto token = eat(lookAhead.type);
			auto left = Unary();
			return new AST(NodeType.UNARY_OP, token.value, left, null);
		}
		return Call();
	}

	/**
	  * Call
	  *   : Primary
	  *   | Primary { (FunctionCall | ArrayAccess | UniversalFunctionCall) }
	  *   | Primary (INC | DEC)
	  *   ;
	  */
	private AST Call()
	out (result) {
		assert(result !is null);
	}
	do {
		auto p = Primary();
		if (lookAhead.type == TokenType.INC || lookAhead.type == TokenType.DEC) {
			auto token = eat(lookAhead.type);
			return new AST(NodeType.UNARY_OP, token.value, p, null);
		}
		if (![TokenType.OPEN_PAR, TokenType.OPEN_BRACKET, TokenType.DOT].canFind(lookAhead.type)) {
			return p;
		}
		AST ast = null;
		while ([TokenType.OPEN_PAR, TokenType.OPEN_BRACKET, TokenType.DOT].canFind(lookAhead.type)) {
			auto left = ast is null ? p : ast;
			if (lookAhead.type == TokenType.OPEN_PAR) {
				ast = FunctionCall(left);
			} else if (lookAhead.type == TokenType.OPEN_BRACKET) {
				ast = ArrayAccess(left);
			} else {
				ast = UniversalFunctionCall(left);
			}
		}
		return ast;
	}

	/**
	  * FunctionCall
	  *   : OPEN_PAR [Arguments] CLOSED_PAR
	  *   ;
	  */
	private AST FunctionCall(AST left) {
		eat(TokenType.OPEN_PAR);
		auto collection = lookAhead.type != TokenType.CLOSED_PAR ? Arguments() : [];
		eat(TokenType.CLOSED_PAR);
		return new AST(NodeType.FUNCTION_CALL, "", left, null, collection);
	}

	/**
	  * ArrayAccess
	  *   : OPEN_BRACKET Expression CLOSED_BRACKET
	  *   ;
	  */
	private AST ArrayAccess(AST left) {
		eat(TokenType.OPEN_BRACKET);
		auto right = Expression();
		eat(TokenType.CLOSED_BRACKET);
		return new AST(NodeType.ARRAY_ACCESS, "", left, right);
	}

	/**
	  * UniversalFunctionCall
	  *   : DOT Identifier OPEN_PAR [Arguments] CLOSED_PAR
	  *   ;
	  */
	private AST UniversalFunctionCall(AST left) {
		eat(TokenType.DOT);
		auto id = Identifier();
		eat(TokenType.OPEN_PAR);
		auto collection = lookAhead.type != TokenType.CLOSED_PAR ? Arguments() : [];
		eat(TokenType.CLOSED_PAR);
		return new AST(NodeType.FUNCTION_CALL, "", id, null, [left] ~ collection);
	}

	/**
	  * Arguments
	  *   : Expression {COMMA Expression}
	  *   ;
	  */
	private AST[] Arguments() {
		auto parameters = [Expression()];
		while (lookAhead.type == TokenType.COMMA) {
			eat(TokenType.COMMA);
			parameters ~= Expression();
		}
		return parameters;
	}

	/**
	 * Primary
	 *   : Number
	 *   | String
	 *   | Boolean
	 *   | Nil
	 *   | Identifier
	 *   | FunctionDeclaration
	 *   | Array
	 *   | OPEN_PAR Expression CLOSED_PAR
	 *   ;
	 */
	private AST Primary() {
		switch (lookAhead.type) {
		case TokenType.NUMBER:
			return Number();
		case TokenType.STRING:
			return String();
		case TokenType.BOOLEAN:
			return Boolean();
		case TokenType.NIL:
			return Nil();
		case TokenType.OPEN_BRACKET:
			return Array();
		case TokenType.FUNCTION_DECL:
			return FunctionDeclaration();
		case TokenType.OPEN_BRACE:
			return ObjectLitteral();
		case TokenType.IDENTIFIER:
			return Identifier();
			//case TokenType.OPEN_PAR:
		default:
			eat(TokenType.OPEN_PAR);
			auto expr = Expression();
			eat(TokenType.CLOSED_PAR);
			return expr;
		}
	}

	/**
	  * Nil
	  *   : NIL
	  *   ;
	  */
	private AST Nil() {
		eat(TokenType.NIL);
		return new AST(NodeType.NIL, "nil");
	}

	/**
	  * Identifier
	  *   : IDENTIFIER
	  *   ;
	  */
	private AST Identifier() {
		auto id = eat(TokenType.IDENTIFIER);
		return new AST(NodeType.IDENTIFIER, id.value, null, null);
	}

	/**
	 * Number
	 *   : NUMBER
	 *   ;
	 */
	private AST Number() {
		auto token = eat(TokenType.NUMBER);
		return new AST(NodeType.NUMBER, token.value);
	}

	/**
	 * String
	 *   : STRING
	 *   ;
	 */
	private AST String() {
		auto token = eat(TokenType.STRING);
		return new AST(NodeType.STRING, token.value);
	}

	/**
	 * Boolean
	 *   : BOOLEAN
	 *   ;
	 */
	private AST Boolean() {
		auto token = eat(TokenType.BOOLEAN);
		return new AST(NodeType.BOOLEAN, token.value);
	}

	/* 
	 * ObjectLitteral
     *   : OPEN_BRACE [AttributeDeclaration {COMMA AttributeDeclaration}] CLOSED_BRACE
     *   ;
	 * 
	 */
	private AST ObjectLitteral() {
		eat(TokenType.OPEN_BRACE);
		AST[] elements = lookAhead.type == TokenType.CLOSED_BRACE ? [] : [AttributeDeclaration()];
		while (lookAhead.type == TokenType.COMMA) {
			eat(TokenType.COMMA);
			elements ~= AttributeDeclaration();
		}
		eat(TokenType.CLOSED_BRACE);
		return new AST(NodeType.OBJECT, elements);
	}

	private AST AttributeDeclaration() {
		auto id = Identifier();
		eat(TokenType.COLON);
		auto value = Expression();
		return new AST(NodeType.IDENTIFIER, "attribute", id, value);
	}

	/**
	 * Array
	 *   : OPEN_BRACKET [Expression {COMMA Expression}] CLOSED_BRACKET
	 *   ;
	 */
	private AST Array() {
		eat(TokenType.OPEN_BRACKET);
		AST[] elements = lookAhead.type == TokenType.CLOSED_BRACKET ? [] : [Expression()];
		while (lookAhead.type == TokenType.COMMA) {
			eat(TokenType.COMMA);
			elements ~= Expression();
		}
		eat(TokenType.CLOSED_BRACKET);
		return new AST(NodeType.ARRAY, elements);
	}

	/**
	 * Consumes a token of the given type from the lexer and returns it
	 *
	 * Throws: Exception if the current token is not of the given type
	 * Returns: The current token if it has the given type
	 */
	private Token eat(TokenType type) {
		scope (exit) {
			lookAhead = lexer.getNextToken();
		}
		auto ret = lookAhead;
		if (ret.type == TokenType.UNKNOWN) {
			ErrorHandler.syntaxError(format("Unknown token %s", ret.value), ret.line, ret.column);
			throw new Exception("hoho");
		} else if (ret.type != type) {
			ErrorHandler.syntaxError(format("Expected %s, got %s", type, ret.type), ret.line, ret.column);
			throw new Exception("hoho");
		}
		return ret;
	}

}

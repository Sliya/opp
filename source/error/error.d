module error.error;

class OppError {
	private string m_type;
	private string m_message;
	private int m_line;
	private int m_column;

	this(string type, string message, int line, int column) {
		this.m_type = type;
		this.m_message = message;
		this.m_line = line;
		this.m_column = column;
	}

	@property string type() {
		return m_type;
	}

	@property string message() {
		return m_message;
	}

	@property int line() {
		return m_line;
	}

	@property int column() {
		return m_column;
	}

}

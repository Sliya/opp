module error.errorHandler;

import error;
import std.stdio : writeln, File;
import std.format : format;

class ErrorHandler {

	private static OppError[] errors;

	public static bool hasErrors() {
		return errors.length > 0;
	}

	public static void syntaxError(string message, int line, int column) {
		errors ~= new OppError("Syntax Error", message, line, column);
	}

	public static void log(File output) {
		foreach (e; errors) {
			output.writeln(format("%s (line %s, column %s) : %s", e.type, e.line, e.column, e.message));
		}
	}

	public static void logAndReset(File output) {
		log(output);
		errors = [];
	}
}

![https://gitlab.com/Sliya/opp/-/commits/develop](https://gitlab.com/Sliya/opp/badges/develop/pipeline.svg) ![https://coveralls.io/gitlab/Sliya/opp?branch=HEAD](https://coveralls.io/repos/gitlab/Sliya/opp/badge.svg?branch=HEAD)
# OPP
opp is a simple language that I developed (using [the D programming language](https://dlang.org/)) for learning purposes. You should probably not use it.

## Language design
At first I wanted to create a purely functional yet easy to use programming language. However, as this is my very first attempt at designing/implementing a language, and as I'm not that familiar with purely functional languages, I changed my mind and opted for something more like a scripting language. Here is what I would like opp to look like:
- Kinda functional (higher order functions, closures);
- Duck typed;
- Kinda OO (classes, inheritance, encapsulation... Not sure about any of that yet, though).


## Big steps
- [x] Basic interpreter
- [x] Can be used to solve small problems
- [ ] Compiled
- [ ] Self-hosted

# Technical stuff

## Compile
To compile, execute `dub build`. It will generate a binary named *opp*.

## Test
`dub -b unittest`
With coverage :
`dub -b unittest-cov`

## Lint
`dub lint -- --styleCheck --skipTests`

## Run
If you have compiled the project before hand, you can simply run `./opp <sourceFile>` or, if you want to use the REPL, `./opp --repl`
If you have not build it, you can run `dub -- <sourceFile>` (or `dub -- --repl` to access the REPL)
